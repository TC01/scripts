#!/usr/bin/env perl

# This is a very thin wrapper around the Verilog::EditFiles package
# from the Verilog::Perl project. This script takes a Verilog source
# file as an argument and splits it into many Verilog source files,
# one per module.

# This may be handy when working with synthesized Verilog (where
# many modules are combined in one file).

# My Perl is really bad. Apologies.

# Requires perl-Verilog-Perl, perl-File-Temp, perl-Getopt-Long.

# Ben Rosser <rosser.bjr@gmail.com>
# Released under the MIT License, please see LICENSE file.

use File::Temp qw(tempfile tempdir);
use Getopt::Long qw(GetOptions);
use Verilog::EditFiles;

# Let's require some command line options.
my $output_path = "processed_rtl";
GetOptions(
	'output=s' => \$output_path
) or die "Usage: $0 [--output PATH] source\n";

# This is apparently sufficient to require one argument after doing
# the Getopt::Long thing.
if (not defined $ARGV[0]) {
    die "Usage: $0 [--output PATH] source\n";
}
my $filename = $ARGV[0];

# Open file and preprocess it.
my @source = "";
if (open my $fh, '+<', $filename) {
	@source = <$fh>;
	close $fh;
} else {
	die "Error: file did not exist."
}

# Preprocess by removing newlines after "module".
# This doesn't really work because it also strips trailing newlines after
# the "endmodule" keywords. So, we'll write this to a temporary file.
foreach(@source) {
	$_ =~ s/(?<=module)\n//;
}

# Re-write source as temp file.
($fh, $tmpname) = tempfile(UNLINK => 1);
print $fh @source;
close $fh;

# Code taken from example snippet:
# https://metacpan.org/pod/Verilog::EditFiles
my $split = Verilog::EditFiles->new(
	outdir => $output_path,
	translate_synthesis => 0,
	lint_header => undef,
	celldefine => 0,
);

# Split the files.
$split->read_and_split($tmpname);
$split->write_files();

