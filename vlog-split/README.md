# vlog-split

This is a Perl program I wrote to split a Verilog source file with multiple
modules defined in it into many Verilog source files, one for each module.

I wrote this in order to better debug the output of a synthesis compiler
(which can produce a single file containing many gate-level modules),
but there may be other applications.

When looking for an existing tool to do this task, I discovered that a Perl
[library](https://metacpan.org/pod/Verilog::EditFiles) existed which solved
the problem. Unfortunately there did not seem to be a pre-existing command
line tool, so I decided to write one.

I then ran into a [small bug](https://www.veripool.org/issues/1311-Verilog-Perl-Verilog-EditFiles-misses-module-declarations-where-module-keyword-and-module-name-are-on-separate-lines-),
which required a workaround. So the script currently pre-processes the
source, writes it to a temporary file, and then runs over that temporary
file.

Disclaimer: I'm not very proficient in Perl. This may not properly handle
all cases.

vlog-split is distributed under the MIT license.

## Requirements

vlog-split.pl requires three Perl libraries:

* Verilog::Perl (Verilog::EditFiles)
* Getopt::Long
* File::Temp

## Usage

Usage is pretty simple. The program takes one positional command line
argument-- the source file to process-- and optionally, the directory
in which output files will be created:

```
vlog-split.pl [-o output] source.v
```
