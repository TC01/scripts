# Miscellaneous Scripts

A wise man (at a [FOSSCON](https://fosscon.us/) lightning talk) once said:
"publish your crappy scripts". This repository is a place for me to do just
that!

Some of these could, potentially, evolve into real projects. Others probably
never will, but just want a version-controlled home. I dislike creating
git repositories for really tiny projects so I decided to make one umbrella
"scripts" repository.

Everything here is licensed under the MIT license, unless otherwise stated.
